<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|--------------------------------------------------------------------------
*/
Route::controller(AuthController::class)->group(function () {
    Route::post("login", "login")->name("login");
    Route::post("register", "register")->name("register");
    Route::post("logout", "logout")->name("logout");
    Route::post("refresh", "refresh")->name("refresh");
});

Route::controller(UserController::class)->group(function () {
    Route::get("user/index", "index")->middleware('can:index')->name("index");
    Route::get("user/show/{id}", "show")->middleware('can:show')->name("show");
    Route::put("user/update", "update")->middleware('can:update')->name("update");
    Route::delete("user/destroy/{id}", "destroy")->middleware('can:destroy')->name("destroy");
});

// Route::resource('users', UserController::class);

Route::post('image/upload', [ImageController::class, 'imageUpload']);

Route::get('mail', [MailController::class, 'sendMail']);