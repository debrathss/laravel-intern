<?php

return [
  "success" => "タスク成功 :name",
  "deliver" => "メールが正常に配信されました",
  "undeliver" => "メールの配信に失敗しました",
];