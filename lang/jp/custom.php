<?php

return [
    "user" => [
        "authentication-failed" => "ユーザー名またはパスワードが無効です。",
    ],
    "response" => [
        "welcome" => "私たちのアプリケーションへようこそ :name",
        "about" => "約",
        "contact" => "コンタクト",
        "phone" => "電話",
        "success" => "タスク成功",
        "failed" => " 失敗した",
        "add" => "正常に追加されました :name",
        "update" => " 正常に更新されました ",
        "delete" => " 正常に削除されました ",
        "create" => " 正常に作成されました ",
        "show" => "正常に取得されました :name",
        "accepted" => ":attribute を受け入れる必要があります",
    ]
];