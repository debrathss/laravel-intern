<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index_route()
    {
        $response = $this->get('user/index');

        $response->assertStatus(200);
    }

    public function test_show_route()
    {
        $response = $this->get('user/show/{id}');

        $response->assertStatus(200);
    }

    public function test_update_route()
    {
        $response = $this->put('user/update');

        $response->assertStatus(200);
    }

    public function test_destroy_route()
    {
        $response = $this->delete('/user/destroy/{id}');

        $response->assertStatus(200);
    }
}