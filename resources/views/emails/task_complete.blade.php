@component('mail::message')

# {{$message}}

Visit gitlab account...

@component('mail::button', ['url' => 'https://gitlab.com/debrathss/laravel-intern/-/tree/final'])
    Visit Gitlab
@endcomponent

Thanks,<br>
Anonymous
@endcomponent