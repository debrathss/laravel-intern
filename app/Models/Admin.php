<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Admin extends Model
{
    use HasFactory;
    protected $fillable = ['email', 'password', 'name', 'phone', 'address'];

    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, 'imageable');
    }

}