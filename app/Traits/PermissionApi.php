<?php
namespace App\Traits;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait HasPermissoinTrait
{
    // Reference for giving the user permission with the help of the Permission trait (Dummy)

    public function givePermissionsTo(array $permissions): bool
    {
        $permissions = $this->getAllPermissions($permissions);
        if ($permissions === null) {
            return $this;
        }
        $this->permissions()->saveMany($permissions);

        return $this;
    }

    public function giveRolesTo(array $roles)
    {
        $roles = $this->getAllRoles($roles);
        if ($roles === null) {
            return $this;
        }
        $this->permissions()->saveMany($roles);

        return $this;
    }

    public function hasPermissionTo($permission): bool
    {
        return $this->hasPermissionThroughRole($permission) || $this->hasPermission($permission);
    }

    public function hasPermissionThroughRole($permission): bool
    {
        foreach ($permission->roles as $role){
            if ($this->roles->contains($role)) {
                return true;
            }
        }

        return false;
    }

    public function hasRole(array $roles): bool
    {
        foreach ($roles as $role) {
            if ($this->roles->contains("slug", $role)) {
                return true;
            }
        }

        return false;
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, "users");
    }

    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class, "roles_permissions");
    }

    protected function hasPermission(object $permission): bool
    {
        return $this->permissions->where("slug", $permission->slug)->count();
    }

    protected function getAllRoles(array $roles): object
    {
        return Role::whereIn("slug", $roles)->get();
    }

    protected function getAllPermissions(array $permissions): object
    {
        return Permission::whereIn("slug", $permissions)->get();
    }
}