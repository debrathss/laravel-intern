<?php

namespace App\Traits;

use App\Models\Image;
use Exception;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;

trait ImageUploadApi
{
    // Made only for the reference if image uploader would be used as the trait....
    public function imageStore(object $request): JsonResponse
    {
        try {
            $validatedData = $request->validate([
                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $validatedData['image'] = $request->file('image')->store('image');
            $data = Image::create($validatedData);
        } catch (Exception $exception) {
            Log::info($exception->getMessage());
            throw $exception;
        }

        return $this->success("Image uploaded", $data, Response::HTTP_CREATED);
    }
}