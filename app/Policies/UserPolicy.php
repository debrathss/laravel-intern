<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
class UserPolicy extends BasePolicy
{
    use HandlesAuthorization;

    public $user;
    public $permission;

    public function __construct(User $user)
    {
        $this->permission = "student";
        $this->user = $user;
        parent::__construct($this->user, $this->permission);
    }
}