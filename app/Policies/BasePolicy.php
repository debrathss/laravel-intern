<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
class BasePolicy
{
    use HandlesAuthorization;

    protected $user;
    protected $permission;

    public function __construct(object $user, $permission)
    {
        $this->user = $user;
        $this->permission = $permission;
    }

    public function index(): bool
    {
        return ($this->user)->permissions()->contains($this->permission);
    }

    public function show(): bool
    {
        return ($this->user)->permissions()->contains($this->permission);
    }

    public function update(): bool
    {
        return ($this->user)->permissions()->contains($this->permission);
    }

    public function destroy(): bool
    {
        return ($this->user)->permissions()->contains($this->permission);
    }
}