<?php

namespace App\Repositories;

use App\Models\Student;

class StudentRepository extends BaseRepository
{
    public function __construct(Student $student)
    {
        $this->model = $student;
        parent::__construct($student);
        $this->rules = [
            'email' => 'required|email|max:255',
            'password' => 'required|max:25',
            'name' => 'max:120',
            'phone' => 'numeric',
            'address' => 'max:255',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ];
    }

}