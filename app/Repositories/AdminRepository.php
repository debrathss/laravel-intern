<?php

namespace App\Repositories;

use App\Models\Admin;

class AdminRepository extends BaseRepository
{
    public function __construct(Admin $admin)
    {
        $this->model = $admin;
        parent::__construct($admin);
        $this->rules = [
            'email' => 'required|email|max:255',
            'password' => 'required|max:25',
            'name' => 'max:120',
            'phone' => 'numeric',
            'address' => 'max:255',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ];
    }

}