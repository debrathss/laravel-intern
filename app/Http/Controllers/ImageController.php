<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function imageUpload(Request $request): JsonResponse
    {
        if (!$request->hasFile('image')) {
            return response()->json(['Uploaded file not found'], 400);
        }

        $allowedfileExtension = ['pdf','jpg','png'];
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        $check = in_array($extension, $allowedfileExtension);

        if ($check) {
            $path = $file->store('public/images');
            $name = $file->getClientOriginalName();
            $save = new Image();
            $save->title = $name;
            $save->path = $path;
            $save->imageable_type = $request->imageable_type;
            $save->imageable_id = $request->imageable_id;
            $save->save();
        } else {
            return response()->json(['invalid_file_format'], 422);
        }

        return response()->json(['file_uploaded'], 200);
    }

}