<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\UserMail;
use App\Repositories\AuthRepository;
use App\Repositories\UserRepository;
use App\Traits\ResponseApi;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    use ResponseApi;

    protected $userRepository;
    protected $authRepository;
    protected string $userName;
    protected string $userEmail;

    public function __construct(UserRepository $userRepository, AuthRepository $authRepository)
    {
        $this->userRepository = $userRepository;
        $this->authRepository = $authRepository;
        $this->userName = Auth::user()->name;
        $this->userEmail = Auth::user()->email;
    }

    public function sendMail(): JsonResponse
    {
        $message = __("mail.success", ['name' => $this->userName]);
        $sendMail = Mail::to($this->userEmail)->send(new UserMail($message));

        if (!empty($sendMail)) {
            return $this->successResponse(__("mail.deliver"));
        } else{
            return $this->successResponse(__("mail.undeliver"));
        }
    }

}