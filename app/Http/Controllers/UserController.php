<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Notifications\UserCrud;
use App\Repositories\UserRepository;
use App\Traits\ResponseAPI;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{
    use ResponseAPI;

    protected $userRepository;
    protected $userResource;
    protected string $userName;
    protected $notify;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->userName = Auth::user()->name;
        $this->notify = Notification::send(request()->user(), new UserCrud());
        // $this->authorizeResource(User::class, "user");
    }

    public function resource(object $data): object
    {
        return new UserResource($data);
    }

    public function collection(object $data): object
    {
        return UserResource::collection($data);
    }

    public function index(): JsonResponse
    {
        try {
            $users = $this->userRepository->fetchAll();
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }

        $this->notify;

        return $this->successResponse(
            message: __("custom.response.success"),
            data: $this->collection($users)
        );
    }

    public function show(int $id): JsonResponse
    {
        try {
            $user = $this->userRepository->fetch($id);
        } catch (Exception $exception) {
            return $this->handleException($exception);
        }

        $this->notify;

        return $this->successResponse(
            message: __("custom.response.show", ['name' => $this->userName]),
            data:  $this->resource($user)
        );
    }

    public function update(Request $request, int $id): JsonResponse
    {
        try {
            $user = $this->userRepository->update($id, $request->all());
        } catch(Exception $exception) {
            return $this->handleException($exception);
        }

        $this->notify;

        return $this->successResponse(
            message: __("custom.response.update", ['name' => $this->userName]),
            data: $this->resourse($user),
        );
    }

    public function destroy(int $id): JsonResponse
    {
        try {
            $user = $this->userRepository->destroy($id);
        } catch(Exception $exception) {
            return $this->handleException($exception);
        }

        $this->notify;

        return $this->successResponse(
            message: __("custom.response.delete", ['name' => $this->userName]),
            data: $this->resource($user)
        );
    }

}