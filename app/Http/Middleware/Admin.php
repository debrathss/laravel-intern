<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class Admin
{
    public function handle(Request $request, Closure $next)
    {
        try {
            if (Auth::user()->role == "admin") {
                return $next($request);
            } else {
                return route("login");
            }
        } catch (Exception $exception) {
            throw $exception;
        }

    }
}