<?php

namespace App\Services;

use App\Repositories\AuthRepository;
class LoginService
{
    // Made only for the reference (Dummy)
    protected $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function login(object $request): array
    {
        $credentials = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        return $this->authRepository->login($credentials);
    }

}