<?php

namespace App\Services;

use App\Repositories\AuthRepository;
use Illuminate\Support\Facades\Hash;

class RegisterService
{
    // Made only for the reference (Dummy)
    protected $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function register(object $request)
    {
        $data = $request->json([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required| string|min:6',
        ]);

        $data['password'] = Hash::make($request->password);

        return $this->authRepository->register($data);
    }
}